using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using Amazon.Lambda.Core;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace StockTransactionApi
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostHandler(StockTransaction transaction, ILambdaContext context)
        {
            var rawContents = JsonConvert.SerializeObject(transaction);
            using (var client = new AmazonKinesisFirehoseClient())
            {
                using (var memStream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(memStream))
                    {
                        writer.Write(rawContents);
                    }

                    await client.PutRecordAsync("input_transactions", new Record() { Data = memStream });
                }
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
