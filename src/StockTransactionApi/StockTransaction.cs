
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace StockTransactionApi
{
    public class StockTransaction
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("share_delta")]
        public int ShareDelta { get; set; }

        [JsonProperty("user")]
        public int UserId { get; set; }

        public override string ToString()
        {
            return $"{Symbol} - {Price}";
        }
    }
}